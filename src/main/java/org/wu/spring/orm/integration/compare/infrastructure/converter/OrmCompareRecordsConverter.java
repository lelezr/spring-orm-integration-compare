package org.wu.spring.orm.integration.compare.infrastructure.converter;

import org.wu.spring.orm.integration.compare.domain.model.orm.compare.records.OrmCompareRecords;
import org.wu.spring.orm.integration.compare.infrastructure.entity.OrmCompareRecordsDO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe Orm 操作记录 
 *
 * @author Jia wei Wu
 * @date 2024/02/28 01:02 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureConverter 
 **/
@Mapper
public interface OrmCompareRecordsConverter {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/
    OrmCompareRecordsConverter INSTANCE = Mappers.getMapper(OrmCompareRecordsConverter.class);
    /**
     * describe 实体对象 转换成领域对象
     *
     * @param ormCompareRecordsDO Orm 操作记录实体对象     
     * @return {@link OrmCompareRecords} Orm 操作记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/
    OrmCompareRecords toOrmCompareRecords(OrmCompareRecordsDO ormCompareRecordsDO);
    /**
     * describe 领域对象 转换成实体对象
     *
     * @param ormCompareRecords Orm 操作记录领域对象     
     * @return {@link OrmCompareRecordsDO} Orm 操作记录实体对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/
     OrmCompareRecordsDO fromOrmCompareRecords(OrmCompareRecords ormCompareRecords); 
}