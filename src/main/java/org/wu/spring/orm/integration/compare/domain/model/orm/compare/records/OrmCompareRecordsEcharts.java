package org.wu.spring.orm.integration.compare.domain.model.orm.compare.records;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * describe Orm 操作记录
 *
 * @author Jia wei Wu
 * @date 2024/02/28 01:02 下午
 **/
@Data
@Accessors(chain = true)
public class OrmCompareRecordsEcharts {

    /**
     * 数据 优先声明属性
     */
    List<List<Object>> data = new ArrayList<>() {
        {
            add(new ArrayList<>() {
                {

                    add("OperationTime");// 执行时间 y
                    add("OperationTime");
                    add("OperationTime");
                    add("ORM"); // ORM
                    add("AffectsRows");// 执行数量 x
                }
            });
        }
    };

    public void addDataList(List<List<Object>> data) {
        if (!ObjectUtils.isEmpty(data)) {
            this.data.addAll(data);
        }

    }

    /**
     * orm 类型
     */
    private List<String> ormList;


}