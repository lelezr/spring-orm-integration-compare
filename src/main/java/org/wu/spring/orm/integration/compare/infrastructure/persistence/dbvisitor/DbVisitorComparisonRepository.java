package org.wu.spring.orm.integration.compare.infrastructure.persistence.dbvisitor;


import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import jakarta.annotation.Resource;
import net.hasor.dbvisitor.lambda.DuplicateKeyStrategy;
import net.hasor.dbvisitor.lambda.EntityQueryOperation;
import net.hasor.dbvisitor.lambda.InsertOperation;
import net.hasor.dbvisitor.lambda.LambdaTemplate;
import net.hasor.dbvisitor.page.Page;
import net.hasor.dbvisitor.page.PageObject;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.infrastructure.converter.SysUserConverter;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;
import org.wu.spring.orm.integration.compare.infrastructure.mapper.dbvisitor.SysUserDbvisitorMapper;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.SysUserRepositoryAbstractRecord;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Orm;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Type;

import java.time.LocalDateTime;
import java.util.List;

/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence
 **/
@Component("dbVisitorComparisonRepository")
public class DbVisitorComparisonRepository extends SysUserRepositoryAbstractRecord {

    @Resource
    SysUserDbvisitorMapper sysUserDbvisitorMapper;

    @Resource
    private LambdaTemplate lambdaTemplate;

    /**
     * describe 新增
     *
     * @param sysUser 新增
     * @return {@link Result<SysUser>} 新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> story(SysUser sysUser) throws Exception {
        boolean success = true;
        super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            // 遇到唯一性索引 用户名、scope
            sysUserDbvisitorMapper.story(sysUserDO);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.DB_VISITOR, Type.story, startTime, endTime, success);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增
     *
     * @param sysUserList 批量新增
     * @return {@link Result<List<SysUser>>} 新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> batchStory(List<SysUser> sysUserList) throws Exception {
        boolean success = true;
        super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        try {
            List<SysUserDO> sysUserDOList = sysUserList.stream().map(SysUserConverter.INSTANCE::fromSysUser).toList();
            InsertOperation<SysUserDO> sysUserDOInsertOperation = lambdaTemplate.lambdaInsert(SysUserDO.class);
            sysUserDOInsertOperation.applyEntity(sysUserDOList).onDuplicateStrategy(DuplicateKeyStrategy.Update).executeSumResult();
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }


        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.DB_VISITOR, Type.batchStory, startTime, endTime, sysUserList.size(), success);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个
     *
     * @param sysUser 查询单个
     * @return {@link Result<SysUser>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> findOne(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findOne(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            EntityQueryOperation<SysUserDO> queryOperation = lambdaTemplate.lambdaQuery(SysUserDO.class);
            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                queryOperation
                        .eq(SysUserDO::getId, sysUser.getId());
            }
            SysUserDO sysUserDO = queryOperation
                    .queryForObject();
            SysUser sysUserOne = SysUserConverter.INSTANCE.toSysUser(sysUserDO);
            return ResultFactory.successOf(sysUserOne);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.DB_VISITOR, Type.findOne, startTime, endTime, success);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 查询多个
     *
     * @param sysUser 查询多个
     * @return {@link Result<List<SysUser>>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> findList(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {


            EntityQueryOperation<SysUserDO> queryOperation = lambdaTemplate.lambdaQuery(SysUserDO.class);
            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                queryOperation
                        .eq(SysUserDO::getId, sysUser.getId());
            }
            List<SysUserDO> sysUserDOList = queryOperation
                    .queryForList();

            List<SysUser> sysUserList = sysUserDOList.stream().map(SysUserConverter.INSTANCE::toSysUser).toList();
            return ResultFactory.successOf(sysUserList);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.DB_VISITOR, Type.findList, startTime, endTime, success);

        }
        return ResultFactory.successOf();

    }

    /**
     * describe 分页查询多个
     *
     * @param size    当前页数
     * @param current 当前页
     * @param sysUser 分页查询多个
     * @return {@link Result<LazyPage<SysUser>>} 分页领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<LazyPage<SysUser>> findPage(int size, int current, SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {

            // 构建分页对象，每页 3 条数据(默认第一页的页码为 0)
            Page pageInfo = new PageObject();
            pageInfo.setPageSize(size);
            pageInfo.setCurrentPage(current - 1);

            // ... 逐一添加

            EntityQueryOperation<SysUserDO> queryOperation = lambdaTemplate.lambdaQuery(SysUserDO.class);
            queryOperation.usePage(pageInfo);

            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                queryOperation
                        .eq(SysUserDO::getId, sysUser.getId());
            }

            if (!ObjectUtils.isEmpty(sysUser.getUsername())) {
                queryOperation
                        .eq(SysUserDO::getUsername, sysUser.getUsername());
            }

            if (!ObjectUtils.isEmpty(sysUser.getIsDeleted())) {
                queryOperation
                        .eq(SysUserDO::getIsDeleted, sysUser.getIsDeleted());
            }

            List<SysUserDO> records = queryOperation
                    .selectAll()
                    .queryForList();

            LazyPage<SysUser> lazyPage = new LazyPage<>(current, size);
            lazyPage.setRecord(records.stream().map(SysUserConverter.INSTANCE::toSysUser).toList());
            return ResultFactory.successOf(lazyPage);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.DB_VISITOR, Type.findPage, startTime, endTime, size, success);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 删除
     *
     * @param sysUser 删除
     * @return {@link Result<SysUser>}
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> remove(SysUser sysUser) throws Exception {
        boolean success = true;
        super.remove(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            sysUserDbvisitorMapper.delete(sysUserDO);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.DB_VISITOR, Type.remove, startTime, endTime, success);
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在
     *
     * @param sysUser 领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<Boolean> exists(SysUser sysUser) throws Exception {
        boolean success = true;
        super.exists(sysUser);
        boolean exists = false;
        LocalDateTime startTime = LocalDateTime.now();
        try {

            EntityQueryOperation<SysUserDO> queryOperation = lambdaTemplate.lambdaQuery(SysUserDO.class);
            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                queryOperation
                        .eq(SysUserDO::getId, sysUser.getId());
            }
            int count = queryOperation.queryForCount();
            // ... 逐一添加
            exists = count != 0;
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }


        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.DB_VISITOR, Type.exists, startTime, endTime, success);
        return ResultFactory.successOf(exists);
    }

}