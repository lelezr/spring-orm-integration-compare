package org.wu.spring.orm.integration.compare.domain.model.sys.user;


import org.springframework.transaction.annotation.Transactional;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;

import java.util.List;

/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository
 **/

public interface ORMRepository {


    /**
     * describe 新增
     *
     * @param sysUser 新增
     * @return {@link  Result<SysUser>} 新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    Result<SysUser> story(SysUser sysUser) throws Exception;

    /**
     * describe 批量新增
     *
     * @param sysUserList 批量新增
     * @return {@link Result<List<SysUser>>} 新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    Result<List<SysUser>> batchStory(List<SysUser> sysUserList) throws Exception;

    /**
     * describe 查询单个
     *
     * @param sysUser 查询单个
     * @return {@link Result<SysUser>} DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    Result<SysUser> findOne(SysUser sysUser) throws Exception;

    /**
     * describe 查询多个
     *
     * @param sysUser 查询多个
     * @return {@link Result<List<SysUser>>} DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    Result<List<SysUser>> findList(SysUser sysUser) throws Exception;

    /**
     * describe 分页查询多个
     *
     * @param size    当前页数
     * @param current 当前页
     * @param sysUser 分页查询多个
     * @return {@link Result< LazyPage <SysUser>>} 分页领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    Result<LazyPage<SysUser>> findPage(int size, int current, SysUser sysUser) throws Exception;

    /**
     * describe 删除
     *
     * @param sysUser 删除
     * @return {@link Result<SysUser>}
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    Result<SysUser> remove(SysUser sysUser) throws Exception;

    /**
     * describe 是否存在
     *
     * @param sysUser 是否存在
     * @return {@link Result<Boolean>} 是否存在
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    Result<Boolean> exists(SysUser sysUser) throws Exception;

}