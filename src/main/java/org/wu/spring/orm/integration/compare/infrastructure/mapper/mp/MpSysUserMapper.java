package org.wu.spring.orm.integration.compare.infrastructure.mapper.mp;

import cn.mybatis.mp.core.mybatis.mapper.MybatisMapper;
import org.apache.ibatis.annotations.Param;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 */
public interface MpSysUserMapper extends MybatisMapper<SysUserDO> {

    void story(@Param("item") SysUserDO sysUserDO);
    /**
     * insert ignore into
     *
     * @param sysUserDOList
     */
    void batchStory(@Param("list") List<SysUserDO> sysUserDOList);
}
