package org.wu.spring.orm.integration.compare.application.impl;

import org.wu.framework.database.lazy.web.plus.stereotype.LazyApplication;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.wu.spring.orm.integration.compare.application.OrmCompareRecordsApplication;
import org.wu.spring.orm.integration.compare.domain.model.orm.compare.records.OrmCompareRecords;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsRemoveCommand;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsStoryCommand;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsUpdateCommand;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsQueryListCommand;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsQueryOneCommand;
import org.wu.spring.orm.integration.compare.application.assembler.OrmCompareRecordsDTOAssembler;
import org.wu.spring.orm.integration.compare.application.dto.OrmCompareRecordsDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;

import jakarta.annotation.Resource;
import org.wu.spring.orm.integration.compare.domain.model.orm.compare.records.OrmCompareRecordsEcharts;
import org.wu.spring.orm.integration.compare.domain.model.orm.compare.records.OrmCompareRecordsRepository;

import java.util.List;

import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Orm;

/**
 * describe Orm 操作记录
 *
 * @author Jia wei Wu
 * @date 2024/02/28 01:02 下午
 * @see org.wu.framework.inner.lazy.persistence.reverse.lazy.ddd.DefaultDDDLazyApplicationImpl
 **/
@LazyApplication
public class OrmCompareRecordsApplicationImpl implements OrmCompareRecordsApplication {

    @Resource
    OrmCompareRecordsRepository ormCompareRecordsRepository;

    /**
     * describe 新增Orm 操作记录
     *
     * @param ormCompareRecordsStoryCommand 新增Orm 操作记录
     * @return {@link Result<OrmCompareRecords>} Orm 操作记录新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    @Override
    public Result<OrmCompareRecords> story(OrmCompareRecordsStoryCommand ormCompareRecordsStoryCommand) {
        OrmCompareRecords ormCompareRecords = OrmCompareRecordsDTOAssembler.INSTANCE.toOrmCompareRecords(ormCompareRecordsStoryCommand);
        return ormCompareRecordsRepository.story(ormCompareRecords);
    }

    /**
     * describe 批量新增Orm 操作记录
     *
     * @param ormCompareRecordsStoryCommandList 批量新增Orm 操作记录
     * @return {@link Result<List<OrmCompareRecords>>} Orm 操作记录新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    @Override
    public Result<List<OrmCompareRecords>> batchStory(List<OrmCompareRecordsStoryCommand> ormCompareRecordsStoryCommandList) {
        List<OrmCompareRecords> ormCompareRecordsList = ormCompareRecordsStoryCommandList.stream().map(OrmCompareRecordsDTOAssembler.INSTANCE::toOrmCompareRecords).collect(Collectors.toList());
        return ormCompareRecordsRepository.batchStory(ormCompareRecordsList);
    }

    /**
     * describe 更新Orm 操作记录
     *
     * @param ormCompareRecordsUpdateCommand 更新Orm 操作记录
     * @return {@link Result<OrmCompareRecords>} Orm 操作记录领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    @Override
    public Result<OrmCompareRecords> updateOne(OrmCompareRecordsUpdateCommand ormCompareRecordsUpdateCommand) {
        OrmCompareRecords ormCompareRecords = OrmCompareRecordsDTOAssembler.INSTANCE.toOrmCompareRecords(ormCompareRecordsUpdateCommand);
        return ormCompareRecordsRepository.story(ormCompareRecords);
    }

    /**
     * describe 查询单个Orm 操作记录
     *
     * @param ormCompareRecordsQueryOneCommand 查询单个Orm 操作记录
     * @return {@link Result<OrmCompareRecordsDTO>} Orm 操作记录DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    @Override
    public Result<OrmCompareRecordsDTO> findOne(OrmCompareRecordsQueryOneCommand ormCompareRecordsQueryOneCommand) {
        OrmCompareRecords ormCompareRecords = OrmCompareRecordsDTOAssembler.INSTANCE.toOrmCompareRecords(ormCompareRecordsQueryOneCommand);
        return ormCompareRecordsRepository.findOne(ormCompareRecords).convert(OrmCompareRecordsDTOAssembler.INSTANCE::fromOrmCompareRecords);
    }

    /**
     * describe 查询多个Orm 操作记录
     *
     * @param ormCompareRecordsQueryListCommand 查询多个Orm 操作记录
     * @return {@link Result<List<OrmCompareRecordsDTO>>} Orm 操作记录DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    @Override
    public Result<List<OrmCompareRecordsDTO>> findList(OrmCompareRecordsQueryListCommand ormCompareRecordsQueryListCommand) {
        OrmCompareRecords ormCompareRecords = OrmCompareRecordsDTOAssembler.INSTANCE.toOrmCompareRecords(ormCompareRecordsQueryListCommand);
        return ormCompareRecordsRepository.findList(ormCompareRecords).convert(ormCompareRecordss -> ormCompareRecordss.stream().map(OrmCompareRecordsDTOAssembler.INSTANCE::fromOrmCompareRecords).collect(Collectors.toList()));
    }

    /**
     * describe 分页查询多个Orm 操作记录
     *
     * @param ormCompareRecordsQueryListCommand 分页查询多个Orm 操作记录
     * @return {@link Result<LazyPage<OrmCompareRecordsDTO>>} 分页Orm 操作记录DTO对象
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    @Override
    public Result<LazyPage<OrmCompareRecordsDTO>> findPage(int size, int current, OrmCompareRecordsQueryListCommand ormCompareRecordsQueryListCommand) {
        OrmCompareRecords ormCompareRecords = OrmCompareRecordsDTOAssembler.INSTANCE.toOrmCompareRecords(ormCompareRecordsQueryListCommand);
        return ormCompareRecordsRepository.findPage(size, current, ormCompareRecords).convert(page -> page.convert(OrmCompareRecordsDTOAssembler.INSTANCE::fromOrmCompareRecords));
    }

    /**
     * describe 删除Orm 操作记录
     *
     * @param ormCompareRecordsRemoveCommand 删除Orm 操作记录
     * @return {@link Result<OrmCompareRecords>} Orm 操作记录
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    @Override
    public Result<OrmCompareRecords> remove(OrmCompareRecordsRemoveCommand ormCompareRecordsRemoveCommand) {
        OrmCompareRecords ormCompareRecords = OrmCompareRecordsDTOAssembler.INSTANCE.toOrmCompareRecords(ormCompareRecordsRemoveCommand);
        return ormCompareRecordsRepository.remove(ormCompareRecords);
    }

    /**
     * describe 获取echarts数据
     */
    @Override
    public Result<OrmCompareRecordsEcharts> findEchartsData(String type) {
        OrmCompareRecordsEcharts ormCompareRecordsEcharts = new OrmCompareRecordsEcharts();
        ormCompareRecordsEcharts.setOrmList(Arrays.stream(Orm.values()).map(Enum::name).toList());
        OrmCompareRecords queryUrmCompareRecordsCondition = new OrmCompareRecords().setType(type);
        //
        List<List<Object>> dataList = ormCompareRecordsRepository.findList(queryUrmCompareRecordsCondition)
                .applyOther(ormCompareRecordsList -> {
                    return ormCompareRecordsList.stream().sorted(Comparator.comparing(OrmCompareRecords::getAffectsRows)).map(ormCompareRecords -> {
                        String orm = ormCompareRecords.getOrm();
                        Integer affectsRows = ormCompareRecords.getAffectsRows();
                        Long operationTime = ormCompareRecords.getOperationTime();
                        List<Object> data = new ArrayList<>();
                        //
                        data.add(operationTime);
                        data.add(operationTime);
                        data.add(operationTime);
                        data.add(orm);
                        data.add(affectsRows);
                        return data;
                    }).toList();

                });
        ormCompareRecordsEcharts.addDataList(dataList);

        return ResultFactory.successOf(ormCompareRecordsEcharts);
    }
}