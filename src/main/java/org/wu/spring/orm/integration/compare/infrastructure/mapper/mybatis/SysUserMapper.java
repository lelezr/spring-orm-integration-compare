package org.wu.spring.orm.integration.compare.infrastructure.mapper.mybatis;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;

import java.util.List;

/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureMapper
 **/
public interface SysUserMapper extends BaseMapper<SysUserDO> {

    void story(@Param("item") SysUserDO sysUserDO);
    void batchStory(@Param("list") List<SysUserDO> sysUserDOList);


}