package org.wu.spring.orm.integration.compare.infrastructure.persistence.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor

public enum Type {
    story("新增或者修改"),
    batchStory("批量新增或者修改"),
    findOne("查询单个"),
    findList("查询所有"),
    findPage("分页"),
    remove("删除"),
    exists("判断是否存在");
    private String desc;

}
