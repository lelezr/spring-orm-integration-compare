package org.wu.spring.orm.integration.compare.application.command.sys.user;

import lombok.Data;
import lombok.experimental.Accessors;
import io.swagger.v3.oas.annotations.media.Schema;
import java.lang.String;
import java.time.LocalDateTime;
import java.lang.Long;
import java.lang.Boolean;
import java.util.Map;
/**
 * describe sys_user 
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyStoryCommand 
 **/
@Data
@Accessors(chain = true)
@Schema(title = "sys_user_story_command",description = "")
public class SysUserStoryCommand {


    /**
     * 
     * 额外字段
     */
    @Schema(description ="额外字段",name ="columnName",example = "")
    private String columnName;

    /**
     * 
     * 创建时间
     */
    @Schema(description ="创建时间",name ="createTime",example = "")
    private LocalDateTime createTime;

    /**
     * 
     * 用户ID
     */
    @Schema(description ="用户ID",name ="id",example = "")
    private Long id;

    /**
     * 
     * null
     */
    @Schema(description ="null",name ="isDeleted",example = "")
    private Boolean isDeleted;

    /**
     * 
     * 密码
     */
    @Schema(description ="密码",name ="password",example = "")
    private String password;

    

  

    /**
     * 
     * null
     */
    @Schema(description ="null",name ="scope",example = "")
    private String scope;

    /**
     * 
     * 状态
     */
    @Schema(description ="状态",name ="status",example = "")
    private Boolean status;

    /**
     * 
     * 更新时间
     */
    @Schema(description ="更新时间",name ="updateTime",example = "")
    private LocalDateTime updateTime;

    /**
     * 
     * 用户名
     */
    @Schema(description ="用户名",name ="username",example = "")
    private String username;
    /**
     *
     * 额外字段1
     */
    @Schema(description ="额外字段1",name ="columnName1",example = "")
    private String columnName_1;
    /**
     *
     * 额外字段2
     */
    @Schema(description ="额外字段2",name ="columnName2",example = "")
    private String columnName_2;
    /**
     *
     * 额外字段3
     */
    @Schema(description ="额外字段3",name ="columnName3",example = "")
    private String columnName_3;
    /**
     *
     * 额外字段4
     */
    @Schema(description ="额外字段4",name ="columnName4",example = "")
    private String columnName_4;
    /**
     *
     * 额外字段5
     */
    @Schema(description ="额外字段5",name ="columnName5",example = "")
    private String columnName_5;
    /**
     *
     * 额外字段6
     */
    @Schema(description ="额外字段6",name ="columnName6",example = "")
    private String columnName_6;
    /**
     *
     * 额外字段7
     */
    @Schema(description ="额外字段7",name ="columnName7",example = "")
    private String columnName_7;
    /**
     *
     * 额外字段8
     */
    @Schema(description ="额外字段8",name ="columnName8",example = "")
    private String columnName_8;
    /**
     *
     * 额外字段9
     */
    @Schema(description ="额外字段9",name ="columnName9",example = "")
    private String columnName_9;
    /**
     *
     * 额外字段10
     */
    @Schema(description ="额外字段10",name ="columnName10",example = "")
    private String columnName_10;
    /**
     *
     * 额外字段11
     */
    @Schema(description ="额外字段11",name ="columnName11",example = "")
    private String columnName_11;
    /**
     *
     * 额外字段12
     */
    @Schema(description ="额外字段12",name ="columnName12",example = "")
    private String columnName_12;
    /**
     *
     * 额外字段13
     */
    @Schema(description ="额外字段13",name ="columnName13",example = "")
    private String columnName_13;
    /**
     *
     * 额外字段14
     */
    @Schema(description ="额外字段14",name ="columnName14",example = "")
    private String columnName_14;
    /**
     *
     * 额外字段15
     */
    @Schema(description ="额外字段15",name ="columnName15",example = "")
    private String columnName_15;
    /**
     *
     * 额外字段16
     */
    @Schema(description ="额外字段16",name ="columnName16",example = "")
    private String columnName_16;
    /**
     *
     * 额外字段17
     */
    @Schema(description ="额外字段17",name ="columnName17",example = "")
    private String columnName_17;
    /**
     *
     * 额外字段18
     */
    @Schema(description ="额外字段18",name ="columnName18",example = "")
    private String columnName_18;
    /**
     *
     * 额外字段19
     */
    @Schema(description ="额外字段19",name ="columnName19",example = "")
    private String columnName_19;
    /**
     *
     * 额外字段20
     */
    @Schema(description ="额外字段20",name ="columnName20",example = "")
    private String columnName_20;
    /**
     *
     * 额外字段21
     */
    @Schema(description ="额外字段21",name ="columnName21",example = "")
    private String columnName_21;
    /**
     *
     * 额外字段22
     */
    @Schema(description ="额外字段22",name ="columnName22",example = "")
    private String columnName_22;
    /**
     *
     * 额外字段23
     */
    @Schema(description ="额外字段23",name ="columnName23",example = "")
    private String columnName_23;
    /**
     *
     * 额外字段24
     */
    @Schema(description ="额外字段24",name ="columnName24",example = "")
    private String columnName_24;
    /**
     *
     * 额外字段25
     */
    @Schema(description ="额外字段25",name ="columnName25",example = "")
    private String columnName_25;
    /**
     *
     * 额外字段26
     */
    @Schema(description ="额外字段26",name ="columnName26",example = "")
    private String columnName_26;
    /**
     *
     * 额外字段27
     */
    @Schema(description ="额外字段27",name ="columnName27",example = "")
    private String columnName_27;
    /**
     *
     * 额外字段28
     */
    @Schema(description ="额外字段28",name ="columnName28",example = "")
    private String columnName_28;
    /**
     *
     * 额外字段29
     */
    @Schema(description ="额外字段29",name ="columnName29",example = "")
    private String columnName_29;
    /**
     *
     * 额外字段30
     */
    @Schema(description ="额外字段30",name ="columnName30",example = "")
    private String columnName_30;
    /**
     *
     * 额外字段31
     */
    @Schema(description ="额外字段31",name ="columnName31",example = "")
    private String columnName_31;
    /**
     *
     * 额外字段32
     */
    @Schema(description ="额外字段32",name ="columnName32",example = "")
    private String columnName_32;
    /**
     *
     * 额外字段33
     */
    @Schema(description ="额外字段33",name ="columnName33",example = "")
    private String columnName_33;
    /**
     *
     * 额外字段34
     */
    @Schema(description ="额外字段34",name ="columnName34",example = "")
    private String columnName_34;
    /**
     *
     * 额外字段35
     */
    @Schema(description ="额外字段35",name ="columnName35",example = "")
    private String columnName_35;
    /**
     *
     * 额外字段36
     */
    @Schema(description ="额外字段36",name ="columnName36",example = "")
    private String columnName_36;
    /**
     *
     * 额外字段37
     */
    @Schema(description ="额外字段37",name ="columnName37",example = "")
    private String columnName_37;
    /**
     *
     * 额外字段38
     */
    @Schema(description ="额外字段38",name ="columnName38",example = "")
    private String columnName_38;
    /**
     *
     * 额外字段39
     */
    @Schema(description ="额外字段39",name ="columnName39",example = "")
    private String columnName_39;
    /**
     *
     * 额外字段40
     */
    @Schema(description ="额外字段40",name ="columnName40",example = "")
    private String columnName_40;

}