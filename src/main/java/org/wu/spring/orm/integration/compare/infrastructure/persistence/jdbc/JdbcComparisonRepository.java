package org.wu.spring.orm.integration.compare.infrastructure.persistence.jdbc;


import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;
import org.wu.framework.core.NormalUsedString;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.SysUserRepositoryAbstractRecord;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Orm;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Type;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence
 **/
@Component("jdbcComparisonRepository")
public class JdbcComparisonRepository extends SysUserRepositoryAbstractRecord {


    @Resource
    private DataSource dataSource;

    /**
     * describe 新增
     *
     * @param sysUser 新增
     * @return {@link Result<SysUser>} 新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> story(SysUser sysUser) throws Exception {
        boolean success = true;
        super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        Connection connection = null;
        try {

            connection = dataSource.getConnection();
            connection.setAutoCommit(true);
            PreparedStatement preparedStatement =
                    connection
                            .prepareStatement(
                                    "INSERT INTO `sys_user` (`column_name`, `create_time`, `id`, `is_deleted`, `password`, `scope`, `status`, `update_time`, `username`,,`column_name_1`,`column_name_2`,`column_name_3`,`column_name_4`,`column_name_5`,`column_name_6`,`column_name_7`,`column_name_8`,`column_name_9`,`column_name_10`,`column_name_11`,`column_name_12`,`column_name_13`,`column_name_14`,`column_name_15`,`column_name_16`,`column_name_17`,`column_name_18`,`column_name_19`,`column_name_20`,`column_name_21`,`column_name_22`,`column_name_23`,`column_name_24`,`column_name_25`,`column_name_26`,`column_name_27`,`column_name_28`,`column_name_29`,`column_name_30`,`column_name_31`,`column_name_32`,`column_name_33`,`column_name_34`,`column_name_35`,`column_name_36`,`column_name_37`,`column_name_38`,`column_name_39`,`column_name_40`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            preparedStatement.setString(1, sysUser.getColumnName());
            preparedStatement.setDate(2, Date.valueOf(sysUser.getCreateTime().toLocalDate()));
            preparedStatement.setLong(3, sysUser.getId());
            preparedStatement.setBoolean(4, sysUser.getIsDeleted());
            preparedStatement.setString(5, sysUser.getPassword());
            preparedStatement.setString(6, sysUser.getScope());
            preparedStatement.setBoolean(7, sysUser.getStatus());
            preparedStatement.setDate(8, Date.valueOf(sysUser.getUpdateTime().toLocalDate()));
            preparedStatement.setString(9, sysUser.getUsername());
            preparedStatement.setString(10, sysUser.getColumnName_1());
            preparedStatement.setString(11, sysUser.getColumnName_2());
            preparedStatement.setString(12, sysUser.getColumnName_3());
            preparedStatement.setString(13, sysUser.getColumnName_4());
            preparedStatement.setString(14, sysUser.getColumnName_5());
            preparedStatement.setString(15, sysUser.getColumnName_6());
            preparedStatement.setString(16, sysUser.getColumnName_7());
            preparedStatement.setString(17, sysUser.getColumnName_8());
            preparedStatement.setString(18, sysUser.getColumnName_9());
            preparedStatement.setString(19, sysUser.getColumnName_10());
            preparedStatement.setString(20, sysUser.getColumnName_11());
            preparedStatement.setString(21, sysUser.getColumnName_12());
            preparedStatement.setString(22, sysUser.getColumnName_13());
            preparedStatement.setString(23, sysUser.getColumnName_14());
            preparedStatement.setString(24, sysUser.getColumnName_15());
            preparedStatement.setString(25, sysUser.getColumnName_16());
            preparedStatement.setString(26, sysUser.getColumnName_17());
            preparedStatement.setString(27, sysUser.getColumnName_18());
            preparedStatement.setString(28, sysUser.getColumnName_19());
            preparedStatement.setString(29, sysUser.getColumnName_20());
            preparedStatement.setString(30, sysUser.getColumnName_21());
            preparedStatement.setString(31, sysUser.getColumnName_22());
            preparedStatement.setString(32, sysUser.getColumnName_23());
            preparedStatement.setString(33, sysUser.getColumnName_24());
            preparedStatement.setString(34, sysUser.getColumnName_25());
            preparedStatement.setString(35, sysUser.getColumnName_26());
            preparedStatement.setString(36, sysUser.getColumnName_27());
            preparedStatement.setString(37, sysUser.getColumnName_28());
            preparedStatement.setString(38, sysUser.getColumnName_29());
            preparedStatement.setString(39, sysUser.getColumnName_30());
            preparedStatement.setString(40, sysUser.getColumnName_31());
            preparedStatement.setString(41, sysUser.getColumnName_32());
            preparedStatement.setString(42, sysUser.getColumnName_33());
            preparedStatement.setString(43, sysUser.getColumnName_34());
            preparedStatement.setString(44, sysUser.getColumnName_35());
            preparedStatement.setString(45, sysUser.getColumnName_36());
            preparedStatement.setString(46, sysUser.getColumnName_37());
            preparedStatement.setString(47, sysUser.getColumnName_38());
            preparedStatement.setString(48, sysUser.getColumnName_39());
            preparedStatement.setString(49, sysUser.getColumnName_40());
            preparedStatement.executeUpdate();
            preparedStatement.close();

        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.JDBC, Type.story, startTime, endTime, success);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增
     *
     * @param sysUserList 批量新增
     * @return {@link Result<List<SysUser>>} 新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> batchStory(List<SysUser> sysUserList) throws Exception {
        boolean success = true;
        super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
//            connection.setAutoCommit(false);
            String sql = "INSERT INTO `sys_user` (`column_name`, `create_time`,  `is_deleted`, `password`, `scope`, `status`, `update_time`, `username`,,`column_name_1`,`column_name_2`,`column_name_3`,`column_name_4`,`column_name_5`,`column_name_6`,`column_name_7`,`column_name_8`,`column_name_9`,`column_name_10`,`column_name_11`,`column_name_12`,`column_name_13`,`column_name_14`,`column_name_15`,`column_name_16`,`column_name_17`,`column_name_18`,`column_name_19`,`column_name_20`,`column_name_21`,`column_name_22`,`column_name_23`,`column_name_24`,`column_name_25`,`column_name_26`,`column_name_27`,`column_name_28`,`column_name_29`,`column_name_30`,`column_name_31`,`column_name_32`,`column_name_33`,`column_name_34`,`column_name_35`,`column_name_36`,`column_name_37`,`column_name_38`,`column_name_39`,`column_name_40`) VALUES";

            String valueSql = sysUserList.stream().parallel().map(sysUser -> "("+
                    sysUser.getColumnName()+"," +
                    sysUser.getCreateTime()+"," +
//                    sysUser.getId()+"," +
                    sysUser.getIsDeleted()+"," +
                    sysUser.getPassword()+"," +
                    sysUser.getScope()+"," +
                    sysUser.getStatus()+"," +
                    sysUser.getUpdateTime()+"," +
                    sysUser.getUsername() + "," +
                    sysUser.getColumnName_1() + "," +
                    sysUser.getColumnName_2() + "," +
                    sysUser.getColumnName_3() + "," +
                    sysUser.getColumnName_4() + "," +
                    sysUser.getColumnName_5() + "," +
                    sysUser.getColumnName_6() + "," +
                    sysUser.getColumnName_7() + "," +
                    sysUser.getColumnName_8() + "," +
                    sysUser.getColumnName_9() + "," +
                    sysUser.getColumnName_10() + "," +
                    sysUser.getColumnName_11() + "," +
                    sysUser.getColumnName_12() + "," +
                    sysUser.getColumnName_13() + "," +
                    sysUser.getColumnName_14() + "," +
                    sysUser.getColumnName_15() + "," +
                    sysUser.getColumnName_16() + "," +
                    sysUser.getColumnName_17() + "," +
                    sysUser.getColumnName_18() + "," +
                    sysUser.getColumnName_19() + "," +
                    sysUser.getColumnName_20() + "," +
                    sysUser.getColumnName_21() + "," +
                    sysUser.getColumnName_22() + "," +
                    sysUser.getColumnName_23() + "," +
                    sysUser.getColumnName_24() + "," +
                    sysUser.getColumnName_25() + "," +
                    sysUser.getColumnName_26() + "," +
                    sysUser.getColumnName_27() + "," +
                    sysUser.getColumnName_28() + "," +
                    sysUser.getColumnName_29() + "," +
                    sysUser.getColumnName_30() + "," +
                    sysUser.getColumnName_31() + "," +
                    sysUser.getColumnName_32() + "," +
                    sysUser.getColumnName_33() + "," +
                    sysUser.getColumnName_34() + "," +
                    sysUser.getColumnName_35() + "," +
                    sysUser.getColumnName_36() + "," +
                    sysUser.getColumnName_37() + "," +
                    sysUser.getColumnName_38() + "," +
                    sysUser.getColumnName_39() + "," +
                    sysUser.getColumnName_40() +

                    ")").collect(Collectors.joining(NormalUsedString.COMMA));
            PreparedStatement preparedStatement =
                    connection
                            .prepareStatement(sql +valueSql);
            preparedStatement.executeLargeBatch();
//            connection.commit();
            preparedStatement.close();
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.JDBC, Type.batchStory, startTime, endTime, sysUserList.size(), success);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个
     *
     * @param sysUser 查询单个
     * @return {@link Result<SysUser>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> findOne(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findOne(sysUser);
        LocalDateTime startTime = LocalDateTime.now();


        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;

        try {

            connection = dataSource.getConnection();

            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT * FROM `sys_user` WHERE `id` = " + sysUser.getId());
            boolean next = resultSet.next();
            SysUser single = new SysUser();
            if (next) {
                String columnName = resultSet.getString("column_name");
                Date createTime = resultSet.getDate("create_time");
                long id = resultSet.getLong("id");
                boolean isDeleted = resultSet.getBoolean("is_deleted");
                String password = resultSet.getString("password");
                String scope = resultSet.getString("scope");
                boolean status = resultSet.getBoolean("status");
                Date updateTime = resultSet.getDate("update_time");
                String username = resultSet.getString("username");
                String columnName_1 = resultSet.getString("column_name_1");
                String columnName_2 = resultSet.getString("column_name_2");
                String columnName_3 = resultSet.getString("column_name_3");
                String columnName_4 = resultSet.getString("column_name_4");
                String columnName_5 = resultSet.getString("column_name_5");
                String columnName_6 = resultSet.getString("column_name_6");
                String columnName_7 = resultSet.getString("column_name_7");
                String columnName_8 = resultSet.getString("column_name_8");
                String columnName_9 = resultSet.getString("column_name_9");
                String columnName_10 = resultSet.getString("column_name_10");
                String columnName_11 = resultSet.getString("column_name_11");
                String columnName_12 = resultSet.getString("column_name_12");
                String columnName_13 = resultSet.getString("column_name_13");
                String columnName_14 = resultSet.getString("column_name_14");
                String columnName_15 = resultSet.getString("column_name_15");
                String columnName_16 = resultSet.getString("column_name_16");
                String columnName_17 = resultSet.getString("column_name_17");
                String columnName_18 = resultSet.getString("column_name_18");
                String columnName_19 = resultSet.getString("column_name_19");
                String columnName_20 = resultSet.getString("column_name_20");
                String columnName_21 = resultSet.getString("column_name_21");
                String columnName_22 = resultSet.getString("column_name_22");
                String columnName_23 = resultSet.getString("column_name_23");
                String columnName_24 = resultSet.getString("column_name_24");
                String columnName_25 = resultSet.getString("column_name_25");
                String columnName_26 = resultSet.getString("column_name_26");
                String columnName_27 = resultSet.getString("column_name_27");
                String columnName_28 = resultSet.getString("column_name_28");
                String columnName_29 = resultSet.getString("column_name_29");
                String columnName_30 = resultSet.getString("column_name_30");
                String columnName_31 = resultSet.getString("column_name_31");
                String columnName_32 = resultSet.getString("column_name_32");
                String columnName_33 = resultSet.getString("column_name_33");
                String columnName_34 = resultSet.getString("column_name_34");
                String columnName_35 = resultSet.getString("column_name_35");
                String columnName_36 = resultSet.getString("column_name_36");
                String columnName_37 = resultSet.getString("column_name_37");
                String columnName_38 = resultSet.getString("column_name_38");
                String columnName_39 = resultSet.getString("column_name_39");
                String columnName_40 = resultSet.getString("column_name_40");

                single.setId(id);
                single.setColumnName(columnName);
                single.setCreateTime(LocalDateTime.ofInstant(createTime.toInstant(), ZoneId.systemDefault()));
                single.setIsDeleted(isDeleted);
                single.setPassword(password);
                single.setScope(scope);
                single.setStatus(status);
                single.setUpdateTime(LocalDateTime.ofInstant(updateTime.toInstant(), ZoneId.systemDefault()));
                single.setUsername(username);
                single.setColumnName_1(columnName_1);
                single.setColumnName_2(columnName_2);
                single.setColumnName_3(columnName_3);
                single.setColumnName_4(columnName_4);
                single.setColumnName_5(columnName_5);
                single.setColumnName_6(columnName_6);
                single.setColumnName_7(columnName_7);
                single.setColumnName_8(columnName_8);
                single.setColumnName_9(columnName_9);
                single.setColumnName_10(columnName_10);
                single.setColumnName_11(columnName_11);
                single.setColumnName_12(columnName_12);
                single.setColumnName_13(columnName_13);
                single.setColumnName_14(columnName_14);
                single.setColumnName_15(columnName_15);
                single.setColumnName_16(columnName_16);
                single.setColumnName_17(columnName_17);
                single.setColumnName_18(columnName_18);
                single.setColumnName_19(columnName_19);
                single.setColumnName_20(columnName_20);
                single.setColumnName_21(columnName_21);
                single.setColumnName_22(columnName_22);
                single.setColumnName_23(columnName_23);
                single.setColumnName_24(columnName_24);
                single.setColumnName_25(columnName_25);
                single.setColumnName_26(columnName_26);
                single.setColumnName_27(columnName_27);
                single.setColumnName_28(columnName_28);
                single.setColumnName_29(columnName_29);
                single.setColumnName_30(columnName_30);
                single.setColumnName_31(columnName_31);
                single.setColumnName_32(columnName_32);
                single.setColumnName_33(columnName_33);
                single.setColumnName_34(columnName_34);
                single.setColumnName_35(columnName_35);
                single.setColumnName_36(columnName_36);
                single.setColumnName_37(columnName_37);
                single.setColumnName_38(columnName_38);
                single.setColumnName_39(columnName_39);
                single.setColumnName_40(columnName_40);
            }
            return ResultFactory.successOf(single);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (resultSet != null) {
                resultSet.close();
            }
            if (connection != null) {
                connection.close();
            }
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.JDBC, Type.findOne, startTime, endTime, success);
        }

        return ResultFactory.successOf();
    }

    /**
     * describe 查询多个
     *
     * @param sysUser 查询多个
     * @return {@link Result<List<SysUser>>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> findList(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);
        LocalDateTime startTime = LocalDateTime.now();


        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;

        try {

            connection = dataSource.getConnection();

            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT * FROM `sys_user` ");

            List<SysUser> sysUserList = new ArrayList<>();

            while (resultSet.next()) {
                SysUser single = new SysUser();
                String columnName = resultSet.getString("column_name");
                Date createTime = resultSet.getDate("create_time");
                long id = resultSet.getLong("id");
                boolean isDeleted = resultSet.getBoolean("is_deleted");
                String password = resultSet.getString("password");
                String scope = resultSet.getString("scope");
                boolean status = resultSet.getBoolean("status");
                Date updateTime = resultSet.getDate("update_time");
                String username = resultSet.getString("username");
                String columnName_1 = resultSet.getString("column_name_1");
                String columnName_2 = resultSet.getString("column_name_2");
                String columnName_3 = resultSet.getString("column_name_3");
                String columnName_4 = resultSet.getString("column_name_4");
                String columnName_5 = resultSet.getString("column_name_5");
                String columnName_6 = resultSet.getString("column_name_6");
                String columnName_7 = resultSet.getString("column_name_7");
                String columnName_8 = resultSet.getString("column_name_8");
                String columnName_9 = resultSet.getString("column_name_9");
                String columnName_10 = resultSet.getString("column_name_10");
                String columnName_11 = resultSet.getString("column_name_11");
                String columnName_12 = resultSet.getString("column_name_12");
                String columnName_13 = resultSet.getString("column_name_13");
                String columnName_14 = resultSet.getString("column_name_14");
                String columnName_15 = resultSet.getString("column_name_15");
                String columnName_16 = resultSet.getString("column_name_16");
                String columnName_17 = resultSet.getString("column_name_17");
                String columnName_18 = resultSet.getString("column_name_18");
                String columnName_19 = resultSet.getString("column_name_19");
                String columnName_20 = resultSet.getString("column_name_20");
                String columnName_21 = resultSet.getString("column_name_21");
                String columnName_22 = resultSet.getString("column_name_22");
                String columnName_23 = resultSet.getString("column_name_23");
                String columnName_24 = resultSet.getString("column_name_24");
                String columnName_25 = resultSet.getString("column_name_25");
                String columnName_26 = resultSet.getString("column_name_26");
                String columnName_27 = resultSet.getString("column_name_27");
                String columnName_28 = resultSet.getString("column_name_28");
                String columnName_29 = resultSet.getString("column_name_29");
                String columnName_30 = resultSet.getString("column_name_30");
                String columnName_31 = resultSet.getString("column_name_31");
                String columnName_32 = resultSet.getString("column_name_32");
                String columnName_33 = resultSet.getString("column_name_33");
                String columnName_34 = resultSet.getString("column_name_34");
                String columnName_35 = resultSet.getString("column_name_35");
                String columnName_36 = resultSet.getString("column_name_36");
                String columnName_37 = resultSet.getString("column_name_37");
                String columnName_38 = resultSet.getString("column_name_38");
                String columnName_39 = resultSet.getString("column_name_39");
                String columnName_40 = resultSet.getString("column_name_40");

                single.setId(id);
                single.setColumnName(columnName);
                single.setCreateTime(createTime.toLocalDate().atTime(LocalTime.MIDNIGHT));
                single.setIsDeleted(isDeleted);
                single.setPassword(password);
                single.setScope(scope);
                single.setStatus(status);
                single.setUpdateTime(updateTime.toLocalDate().atTime(LocalTime.MIDNIGHT));
                single.setUsername(username);
                single.setColumnName_1(columnName_1);
                single.setColumnName_2(columnName_2);
                single.setColumnName_3(columnName_3);
                single.setColumnName_4(columnName_4);
                single.setColumnName_5(columnName_5);
                single.setColumnName_6(columnName_6);
                single.setColumnName_7(columnName_7);
                single.setColumnName_8(columnName_8);
                single.setColumnName_9(columnName_9);
                single.setColumnName_10(columnName_10);
                single.setColumnName_11(columnName_11);
                single.setColumnName_12(columnName_12);
                single.setColumnName_13(columnName_13);
                single.setColumnName_14(columnName_14);
                single.setColumnName_15(columnName_15);
                single.setColumnName_16(columnName_16);
                single.setColumnName_17(columnName_17);
                single.setColumnName_18(columnName_18);
                single.setColumnName_19(columnName_19);
                single.setColumnName_20(columnName_20);
                single.setColumnName_21(columnName_21);
                single.setColumnName_22(columnName_22);
                single.setColumnName_23(columnName_23);
                single.setColumnName_24(columnName_24);
                single.setColumnName_25(columnName_25);
                single.setColumnName_26(columnName_26);
                single.setColumnName_27(columnName_27);
                single.setColumnName_28(columnName_28);
                single.setColumnName_29(columnName_29);
                single.setColumnName_30(columnName_30);
                single.setColumnName_31(columnName_31);
                single.setColumnName_32(columnName_32);
                single.setColumnName_33(columnName_33);
                single.setColumnName_34(columnName_34);
                single.setColumnName_35(columnName_35);
                single.setColumnName_36(columnName_36);
                single.setColumnName_37(columnName_37);
                single.setColumnName_38(columnName_38);
                single.setColumnName_39(columnName_39);
                single.setColumnName_40(columnName_40);
                sysUserList.add(single);
            }

            return ResultFactory.successOf(sysUserList);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (resultSet != null) {
                resultSet.close();
            }
            if (connection != null) {
                connection.close();
            }
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.JDBC, Type.findList, startTime, endTime, success);
        }

        return ResultFactory.successOf();

    }

    /**
     * describe 分页查询多个
     *
     * @param size    当前页数
     * @param current 当前页
     * @param sysUser 分页查询多个
     * @return {@link Result<LazyPage<SysUser>>} 分页领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<LazyPage<SysUser>> findPage(int size, int current, SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);

        LocalDateTime startTime = LocalDateTime.now();


        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;

        try {

            connection = dataSource.getConnection();

            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT * FROM `sys_user` limit " + current + "," + size);

            List<SysUser> sysUserList = new ArrayList<>();

            while (resultSet.next()) {
                SysUser single = new SysUser();
                String columnName = resultSet.getString("column_name");
                Date createTime = resultSet.getDate("create_time");
                long id = resultSet.getLong("id");
                boolean isDeleted = resultSet.getBoolean("is_deleted");
                String password = resultSet.getString("password");
                String scope = resultSet.getString("scope");
                boolean status = resultSet.getBoolean("status");
                Date updateTime = resultSet.getDate("update_time");
                String username = resultSet.getString("username");

                String columnName_1 = resultSet.getString("column_name_1");
                String columnName_2 = resultSet.getString("column_name_2");
                String columnName_3 = resultSet.getString("column_name_3");
                String columnName_4 = resultSet.getString("column_name_4");
                String columnName_5 = resultSet.getString("column_name_5");
                String columnName_6 = resultSet.getString("column_name_6");
                String columnName_7 = resultSet.getString("column_name_7");
                String columnName_8 = resultSet.getString("column_name_8");
                String columnName_9 = resultSet.getString("column_name_9");
                String columnName_10 = resultSet.getString("column_name_10");
                String columnName_11 = resultSet.getString("column_name_11");
                String columnName_12 = resultSet.getString("column_name_12");
                String columnName_13 = resultSet.getString("column_name_13");
                String columnName_14 = resultSet.getString("column_name_14");
                String columnName_15 = resultSet.getString("column_name_15");
                String columnName_16 = resultSet.getString("column_name_16");
                String columnName_17 = resultSet.getString("column_name_17");
                String columnName_18 = resultSet.getString("column_name_18");
                String columnName_19 = resultSet.getString("column_name_19");
                String columnName_20 = resultSet.getString("column_name_20");
                String columnName_21 = resultSet.getString("column_name_21");
                String columnName_22 = resultSet.getString("column_name_22");
                String columnName_23 = resultSet.getString("column_name_23");
                String columnName_24 = resultSet.getString("column_name_24");
                String columnName_25 = resultSet.getString("column_name_25");
                String columnName_26 = resultSet.getString("column_name_26");
                String columnName_27 = resultSet.getString("column_name_27");
                String columnName_28 = resultSet.getString("column_name_28");
                String columnName_29 = resultSet.getString("column_name_29");
                String columnName_30 = resultSet.getString("column_name_30");
                String columnName_31 = resultSet.getString("column_name_31");
                String columnName_32 = resultSet.getString("column_name_32");
                String columnName_33 = resultSet.getString("column_name_33");
                String columnName_34 = resultSet.getString("column_name_34");
                String columnName_35 = resultSet.getString("column_name_35");
                String columnName_36 = resultSet.getString("column_name_36");
                String columnName_37 = resultSet.getString("column_name_37");
                String columnName_38 = resultSet.getString("column_name_38");
                String columnName_39 = resultSet.getString("column_name_39");
                String columnName_40 = resultSet.getString("column_name_40");

                single.setId(id);
                single.setColumnName(columnName);
                single.setCreateTime(createTime.toLocalDate().atTime(LocalTime.MIDNIGHT));
                single.setIsDeleted(isDeleted);
                single.setPassword(password);
                single.setScope(scope);
                single.setStatus(status);
                single.setUpdateTime(updateTime.toLocalDate().atTime(LocalTime.MIDNIGHT));
                single.setUsername(username);
                single.setColumnName_1(columnName_1);
                single.setColumnName_2(columnName_2);
                single.setColumnName_3(columnName_3);
                single.setColumnName_4(columnName_4);
                single.setColumnName_5(columnName_5);
                single.setColumnName_6(columnName_6);
                single.setColumnName_7(columnName_7);
                single.setColumnName_8(columnName_8);
                single.setColumnName_9(columnName_9);
                single.setColumnName_10(columnName_10);
                single.setColumnName_11(columnName_11);
                single.setColumnName_12(columnName_12);
                single.setColumnName_13(columnName_13);
                single.setColumnName_14(columnName_14);
                single.setColumnName_15(columnName_15);
                single.setColumnName_16(columnName_16);
                single.setColumnName_17(columnName_17);
                single.setColumnName_18(columnName_18);
                single.setColumnName_19(columnName_19);
                single.setColumnName_20(columnName_20);
                single.setColumnName_21(columnName_21);
                single.setColumnName_22(columnName_22);
                single.setColumnName_23(columnName_23);
                single.setColumnName_24(columnName_24);
                single.setColumnName_25(columnName_25);
                single.setColumnName_26(columnName_26);
                single.setColumnName_27(columnName_27);
                single.setColumnName_28(columnName_28);
                single.setColumnName_29(columnName_29);
                single.setColumnName_30(columnName_30);
                single.setColumnName_31(columnName_31);
                single.setColumnName_32(columnName_32);
                single.setColumnName_33(columnName_33);
                single.setColumnName_34(columnName_34);
                single.setColumnName_35(columnName_35);
                single.setColumnName_36(columnName_36);
                single.setColumnName_37(columnName_37);
                single.setColumnName_38(columnName_38);
                single.setColumnName_39(columnName_39);
                single.setColumnName_40(columnName_40);
                sysUserList.add(single);
            }

            LazyPage<SysUser> lazyPage = LazyPage.of(current, size);
            lazyPage.setRecord(sysUserList);
            return ResultFactory.successOf(lazyPage);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (resultSet != null) {
                resultSet.close();
            }
            if (connection != null) {
                connection.close();
            }
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.JDBC, Type.findPage, startTime, endTime, size,success);
        }

        return ResultFactory.successOf();
    }

    /**
     * describe 删除
     *
     * @param sysUser 删除
     * @return {@link Result<SysUser>}
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> remove(SysUser sysUser) throws Exception {
        boolean success = true;
        super.remove(sysUser);
        LocalDateTime startTime = LocalDateTime.now();


        Connection connection = null;
        Statement statement = null;

        try {

            connection = dataSource.getConnection();

            statement = connection.createStatement();

            statement.executeUpdate("delete from `sys_user` where id = " + sysUser.getId());


        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.JDBC, Type.remove, startTime, endTime, success);
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在
     *
     * @param sysUser 领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<Boolean> exists(SysUser sysUser) throws Exception {
        boolean success = true;
        super.exists(sysUser);
        boolean exists = false;
        LocalDateTime startTime = LocalDateTime.now();


        Connection connection = null;
        Statement statement = null;

        try {

            connection = dataSource.getConnection();

            statement = connection.createStatement();

            statement.executeQuery("select count(1) from `sys_user` where id = " + sysUser.getId());


        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.JDBC, Type.exists, startTime, endTime, success);
        return ResultFactory.successOf(exists);
    }

}