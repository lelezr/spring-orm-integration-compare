package org.wu.spring.orm.integration.compare.infrastructure.persistence.lazy;

import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.ORMComparisonRepository;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.infrastructure.converter.SysUserConverter;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.SysUserRepositoryAbstractRecord;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Orm;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Type;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence
 **/
@Component("lazy")
public  class LazyComparisonRepository extends SysUserRepositoryAbstractRecord implements ORMComparisonRepository {
    @Resource
    LazyLambdaStream lazyLambdaStream;


    /**
     * describe 新增
     *
     * @param sysUser 新增
     * @return {@link Result<SysUser>} 新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> story(SysUser sysUser) throws Exception {
        boolean success = true;

        super.resetTestTableRecords();

        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            lazyLambdaStream.upsert(sysUserDO);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.LAZY, Type.story, startTime, endTime, success);

        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增
     *
     * @param sysUserList 批量新增
     * @return {@link Result<List<SysUser>>} 新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> batchStory(List<SysUser> sysUserList) throws Exception {
        boolean success = true;

                super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        try {
            List<SysUserDO> sysUserDOList = sysUserList.stream().map(SysUserConverter.INSTANCE::fromSysUser).collect(Collectors.toList());
            lazyLambdaStream.upsert(sysUserDOList);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.LAZY, Type.batchStory, startTime, endTime, sysUserList.size(), success);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个
     *
     * @param sysUser 查询单个
     * @return {@link Result<SysUser>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> findOne(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findOne(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        SysUser sysUserOne = null;
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            sysUserOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(sysUserDO), SysUser.class);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.LAZY, Type.findOne, startTime, endTime, success);
        return ResultFactory.successOf(sysUserOne);
    }

    /**
     * describe 查询多个
     *
     * @param sysUser 查询多个
     * @return {@link Result<List<SysUser>>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> findList(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            List<SysUser> sysUserList = lazyLambdaStream.selectList(LazyWrappers.lambdaWrapperBean(sysUserDO), SysUser.class);
            return ResultFactory.successOf(sysUserList);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
            return ResultFactory.errorOf();
        }finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.LAZY, Type.findList, startTime, endTime, success);
        }


    }

    /**
     * describe 分页查询多个
     *
     * @param size    当前页数
     * @param current 当前页
     * @param sysUser 分页查询多个
     * @return {@link Result<LazyPage<SysUser>>} 分页领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<LazyPage<SysUser>> findPage(int size, int current, SysUser sysUser) throws Exception {
        boolean success = true;
        super.findPage(size, current, sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            LazyPage<SysUser> lazyPage = new LazyPage<>(current, size);
            LazyPage<SysUser> sysUserLazyPage = lazyLambdaStream.selectPage(LazyWrappers.lambdaWrapperBean(sysUserDO), lazyPage, SysUser.class);
            return ResultFactory.successOf(sysUserLazyPage);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;

            return ResultFactory.errorOf();
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.LAZY, Type.findPage, startTime, endTime, size,success);
        }


    }

    /**
     * describe 删除
     *
     * @param sysUser 删除
     * @return {@link Result<SysUser>}
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> remove(SysUser sysUser) throws Exception {
        boolean success = true;
        super.remove(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(sysUserDO));
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.LAZY, Type.remove, startTime, endTime, success);
        return ResultFactory.errorOf();
    }

    /**
     * describe 是否存在
     *
     * @param sysUser 领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<Boolean> exists(SysUser sysUser) throws Exception {
        boolean success = true;
        Boolean exists=false;
        super.exists(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
             exists = lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(sysUserDO));
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.LAZY, Type.exists, startTime, endTime, success);
        return ResultFactory.successOf(exists);
    }

}