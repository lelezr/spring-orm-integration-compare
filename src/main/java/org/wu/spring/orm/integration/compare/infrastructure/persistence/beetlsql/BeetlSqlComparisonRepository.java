package org.wu.spring.orm.integration.compare.infrastructure.persistence.beetlsql;


import org.wu.framework.core.utils.LazyListUtils;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;

import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import jakarta.annotation.Resource;
import net.hasor.dbvisitor.page.Page;
import net.hasor.dbvisitor.page.PageObject;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.page.PageResult;
import org.beetl.sql.core.query.LambdaQuery;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.infrastructure.converter.SysUserConverter;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;
import org.wu.spring.orm.integration.compare.infrastructure.mapper.beetlsql.SysUserBeetlSqlMapper;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.SysUserRepositoryAbstractRecord;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Orm;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Type;

import java.time.LocalDateTime;
import java.util.List;

/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence
 **/
@Component("beetlSqlComparisonRepository")
public class BeetlSqlComparisonRepository extends SysUserRepositoryAbstractRecord {

    @Resource
    SysUserBeetlSqlMapper sysUserBeetlSqlMapper;


    /**
     * describe 新增
     *
     * @param sysUser 新增
     * @return {@link Result <SysUser>} 新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> story(SysUser sysUser) throws Exception {
        boolean success = true;
        super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            // 遇到唯一性索引 用户名、scope
            sysUserBeetlSqlMapper.insert(sysUserDO);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.BEETL_SQL, Type.story, startTime, endTime, success);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增
     *
     * @param sysUserList 批量新增
     * @return {@link Result<List<SysUser>>} 新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> batchStory(List<SysUser> sysUserList) throws Exception {
        boolean success = true;
        super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        try {
            List<SysUserDO> sysUserDOList = sysUserList.stream().map(SysUserConverter.INSTANCE::fromSysUser).toList();
            // 分组处理
            LazyListUtils.splitListThen(sysUserDOList,10000,sysUserDOS -> sysUserBeetlSqlMapper.insertBatch(sysUserDOS));

        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }


        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.BEETL_SQL, Type.batchStory, startTime, endTime, sysUserList.size(), success);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个
     *
     * @param sysUser 查询单个
     * @return {@link Result<SysUser>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> findOne(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findOne(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            LambdaQuery<SysUserDO> sysUserDOLambdaQuery = sysUserBeetlSqlMapper.getSQLManager().lambdaQuery(SysUserDO.class);

            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                sysUserDOLambdaQuery
                        .andEq(SysUserDO::getId, sysUser.getId());
            }
            List<SysUserDO> sysUserDOList = sysUserDOLambdaQuery
                    .selectSimple();

            SysUser sysUserOne = SysUserConverter.INSTANCE.toSysUser(sysUserDOList.get(0));
            return ResultFactory.successOf(sysUserOne);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.BEETL_SQL, Type.findOne, startTime, endTime, success);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 查询多个
     *
     * @param sysUser 查询多个
     * @return {@link Result<List<SysUser>>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> findList(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {


            LambdaQuery<SysUserDO> sysUserDOLambdaQuery = sysUserBeetlSqlMapper.getSQLManager().lambdaQuery(SysUserDO.class);

            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                sysUserDOLambdaQuery
                        .andEq(SysUserDO::getId, sysUser.getId());
            }

            if (!ObjectUtils.isEmpty(sysUser.getIsDeleted())) {
                sysUserDOLambdaQuery
                        .andEq(SysUserDO::getIsDeleted, sysUser.getIsDeleted());
            }
            if (!ObjectUtils.isEmpty(sysUser.getUsername())) {
                sysUserDOLambdaQuery
                        .andEq(SysUserDO::getUsername, sysUser.getUsername());
            }


            List<SysUserDO> sysUserDOList = sysUserDOLambdaQuery
                    .selectSimple();

            List<SysUser> sysUserList = sysUserDOList.stream().map(SysUserConverter.INSTANCE::toSysUser).toList();
            return ResultFactory.successOf(sysUserList);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.BEETL_SQL, Type.findList, startTime, endTime, success);

        }
        return ResultFactory.successOf();

    }

    /**
     * describe 分页查询多个
     *
     * @param size    当前页数
     * @param current 当前页
     * @param sysUser 分页查询多个
     * @return {@link Result<LazyPage<SysUser>>} 分页领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<LazyPage<SysUser>> findPage(int size, int current, SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {

            // 构建分页对象，每页 3 条数据(默认第一页的页码为 0)
            Page pageInfo = new PageObject();
            pageInfo.setPageSize(size);
            pageInfo.setCurrentPage(current);

            // ... 逐一添加
            SQLManager sqlManager = sysUserBeetlSqlMapper.getSQLManager();

            LambdaQuery<SysUserDO> sysUserDOLambdaQuery = sqlManager.lambdaQuery(SysUserDO.class);

            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                sysUserDOLambdaQuery
                        .andEq(SysUserDO::getId, sysUser.getId());
            }
            PageResult<SysUserDO> sysUserDOPageResult =
                    sysUserDOLambdaQuery
                            .page(current, size);


            LazyPage<SysUser> lazyPage = new LazyPage<>(current, size);
            lazyPage.setTotal(sysUserDOPageResult.getTotalRow());
            lazyPage.setRecord(sysUserDOPageResult.getList().stream().map(SysUserConverter.INSTANCE::toSysUser).toList());
            return ResultFactory.successOf(lazyPage);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.BEETL_SQL, Type.findPage, startTime, endTime, size, success);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 删除
     *
     * @param sysUser 删除
     * @return {@link Result<SysUser>}
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> remove(SysUser sysUser) throws Exception {
        boolean success = true;
        super.remove(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            LambdaQuery<SysUserDO> sysUserDOLambdaQuery = sysUserBeetlSqlMapper.getSQLManager().lambdaQuery(SysUserDO.class);

            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                sysUserDOLambdaQuery
                        .andEq(SysUserDO::getId, sysUser.getId());
            }
            sysUserDOLambdaQuery.delete();
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.BEETL_SQL, Type.remove, startTime, endTime, success);
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在
     *
     * @param sysUser 领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<Boolean> exists(SysUser sysUser) throws Exception {
        boolean success = true;
        super.exists(sysUser);
        boolean exists = false;
        LocalDateTime startTime = LocalDateTime.now();
        try {

            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            LambdaQuery<SysUserDO> sysUserDOLambdaQuery = sysUserBeetlSqlMapper.getSQLManager().lambdaQuery(SysUserDO.class);

            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                sysUserDOLambdaQuery
                        .andEq(SysUserDO::getId, sysUser.getId());
            }

            long count = sysUserDOLambdaQuery.count();
            // ... 逐一添加
            exists = count != 0L;
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }


        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.BEETL_SQL, Type.exists, startTime, endTime, success);
        return ResultFactory.successOf(exists);
    }

}