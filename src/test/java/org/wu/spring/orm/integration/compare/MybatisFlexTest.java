package org.wu.spring.orm.integration.compare;


import com.mybatisflex.codegen.Generator;
import com.mybatisflex.codegen.config.GlobalConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.util.function.UnaryOperator;

public class MybatisFlexTest {


    public static void main(String[] args) {

        //配置数据源
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/acw?characterEncoding=utf-8");
        dataSource.setUsername("root");
        dataSource.setPassword("wujiawei");

        GlobalConfig globalConfig = new GlobalConfig();

        //用户信息表，用于存放用户信息。 -> 用户信息
        UnaryOperator<String> tableFormat = (e) -> e.split("，")[0].replace("表", "");

        //设置注解生成配置
        globalConfig.getJavadocConfig()
                .setAuthor("王帅")
                .setTableCommentFormat(tableFormat);

        //设置生成文件目录和根包
        globalConfig.getPackageConfig()
                .setSourceDir(System.getProperty("user.dir") + "/src/test/java")
                .setMapperXmlPath(System.getProperty("user.dir") + "/src/test/resources/mapper")
                .setBasePackage("com.test");

        //设置表前缀和只生成哪些表
        globalConfig.getStrategyConfig()
                .setTablePrefix("sys_")
                .setGenerateTable("sys_user");

        //设置模板路径
        //globalConfig.getTemplateConfig()
        //        .setEntity("D:\\Documents\\配置文件\\entity.tpl");

        //配置生成 entity
        globalConfig.enableEntity()
                .setOverwriteEnable(true)
                .setWithLombok(true)
                .setWithSwagger(true);

        //配置生成 mapper
        globalConfig.enableMapper();
        //配置生成 service
        globalConfig.enableService();
        //配置生成 serviceImpl
        globalConfig.enableServiceImpl()
//            .setSuperClass(CacheableServiceImpl.class)
                .setCacheExample(true);
        //配置生成 controller
        globalConfig.enableController();
        //配置生成 tableDef
        globalConfig.enableTableDef();
        //配置生成 mapperXml
        globalConfig.enableMapperXml();
        //配置生成 package-info.java
        globalConfig.enablePackageInfo();

        //通过 datasource 和 globalConfig 创建代码生成器
        Generator generator = new Generator(dataSource, globalConfig);

        //开始生成代码
        generator.generate();
    }
}
